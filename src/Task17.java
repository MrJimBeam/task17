import java.lang.invoke.SwitchPoint;
import java.util.Scanner;

public class Task17 {
    public static void main(String[] args) {
        Person user = new Person("Jim", "Beam", 100, 12345678, 1000, 98765432, 10000);
        int choice = 0;
        int cardChoice = 0;
        Scanner mysC = new Scanner(System.in);
        System.out.println("The Jack Daniels costs 50, please select payment method:");
        System.out.println("1. Cash");
        System.out.println("2. Card");
        choice = mysC.nextInt();

        switch (choice){
            case 1:
                if(user.getCash().pay(50) != -1){
                    System.out.println("Your change is " + user.getCash().getAmount());
                }else {
                    System.out.println("That was insufficient amount of cash, you require " + (50-user.getCash().getAmount()) + "more");
                }
                break;
            case 2:
                System.out.println("Please select a card to pay with:");
                System.out.println("1. Credit Card");
                System.out.println("2. Saving Card");
                cardChoice = mysC.nextInt();
                break;
        }
        switch (cardChoice){
            case 1:
                if(user.getCreditCard().pay(50) != -1){
                    System.out.println("Your credit is " + user.getCreditCard().getCredit());
                }else {
                    System.out.println("That was insufficient amount of credit, you require " + (50-user.getCreditCard().getCredit()) + "more");
                }
                break;
            case 2:
                if(user.getSavingsCard().pay(50) != -1){
                    System.out.println("Your savings are " + user.getSavingsCard().getSavings());
                }else {
                    System.out.println("That was insufficient amount of savings, you require " + (50-user.getSavingsCard().getSavings()) + "more");
                }
                break;
        }
    }
}
