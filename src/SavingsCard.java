public class SavingsCard extends Card{
    private int savings = 0;

    @Override
    public int pay(int sum) {
        if(sum <= savings){
            savings = savings-sum;
            return savings;
        }else{
            return -1;
        }
    }

    public SavingsCard(int savings, int cardNR) {
        super(cardNR);
        this.savings = savings;
    }

    public int getSavings() {
        return savings;
    }
}
