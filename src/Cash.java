public class Cash {
    private int amount = 0;

    public Cash(int amount) {
        this.amount = amount;
    }

    public int pay(int sum){
        if(amount >= sum){
            amount = amount-sum;
            return amount;
        }else{
            return -1;
        }
    }

    public int getAmount() {
        return amount;
    }
}
