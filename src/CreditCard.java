public class CreditCard extends Card {
    private int credit = 0;

    @Override
    public int pay(int sum ) {
        if(sum <= credit){
            credit = credit-sum;
            return credit;
        }else{
            return -1;
        }
    }

    public CreditCard(int credit, int cardNR) {
        super(cardNR);
        this.credit = credit;

    }

    public int getCredit() {
        return credit;
    }
}
