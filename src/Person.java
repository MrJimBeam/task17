public class Person {
    private String name;
    private String lastName;
    private Cash cash;
    private CreditCard creditCard;
    private SavingsCard savingsCard;

    public Person(String name, String lastName, int cash, int creditNr, int credit, int savingsNR, int savings) {
        this.name = name;
        this.lastName = lastName;
        this.cash = new Cash(cash);
        this.creditCard = new CreditCard(credit, creditNr);
        this.savingsCard = new SavingsCard(savings, savingsNR);
    }

    public Cash getCash() {
        return cash;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public SavingsCard getSavingsCard() {
        return savingsCard;
    }
}
